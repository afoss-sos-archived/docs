# Game Design Document Outline

This game design document is a blueprint from wich a game is to be built. As such, every single detail necessary to build the game should be addressed. The larger the team and the longer the design and development cycle, the more critical is the need.


## 1. Title Page   
### 1.1. Game Name
Shadows of Shaema (SOS)
## 2. Game Overview
### 2.1. Game Concept
Shadows of Shaema (from now on referred simply as SOS) will be a massive multiplayer online game where players can interact with the world in many ways that other games don't currently offer.
### 2.2. Genre
MMORPG (Massive Multiplayer Online Role Playing Game)
### 2.3. Target Audience
Since SOS will feature fighting, monsters and interaction with other players, it shouldn't be targeted at very young audiences. Suggested target: +12
### 2.4. Game Flow Summary
Players in SOS will explore the world as they want. There will be quests to be completed, secrets to be found, battles to be fought and players to be met.
### 2.5. Look and Feel
The game will look like 2D, with a retro style but keeping the art fresh to withstand with the competitors.
## 3. Gameplay and  Mechanics
### 3.1. Gameplay
#### 3.1.1. Game Progression
SOS will feature a XP mechanic: the more a player does a certain thing, the more his level increases and the more efficient he becomes in doing said thing. 
#### 3.1.2. Mission/challenge Structure
The player will have several ways to enjoy the game. He can explore the history and missions of the game, explore the world, or become popular with various social mechanics.
#### 3.1.3. Puzzle Structure
Puzzles will be mainly concentrated on dungeons. Since it's not the focus of the game, the puzzles will be easy to medium difficulty, taking no more than 15 minutes for the player to complete them.
#### 3.1.4. Objectives
SOS will have as the main objective fight the dark forces of the Dark Gods. As such there will be challenging bosses which can only be defeated with proper teamwork and communication between players.
#### 3.1.5. Play Flow
SOS will not lock the player to certain tasks. The player is free to do whatever he wants. As such, we want to implement complete and pure free roaming, even if the player shouldn't be travelling to certain dangerous places.
### 3.2. Mechanics
#### 3.2.1. Physics
Since it's a 2D game, there won't be many physics present in the game. Mainly dropping items, knockbacking players/enemies, and so on.
#### 3.2.2. Movement in the game
Movement will be the traditional WASD and/or ULDR keys. Players will be able to travel in different ways, such as by horse, flying animals or public transportation (such as trains). Fast travel will be limited and will cost players valuable resources, to reenforce travelling and exploring the world.
#### 3.2.3. Objects
Most objects in the overworld will be able to be collected (eg. chopping down trees, breaking stones, etc...). In the cities, to prevent griefing, most objects won't be affected by player's actions. Objects that can be collected will drop various items to the player.
#### 3.2.4. Actions
Players will be able to interact in cities with various local and global events. In the overworld there will be many gameplay details for the player to notice.
#### 3.2.5. Combat
Combat will be in real-time and focused on magic. SOS will inovate with a module based magic system, which allows players to build their own spells with an incredible level of detail (range, power, buffs/debuffs, etc...)
#### 3.2.6. Economy
There will be markets in the cities, either run by NPC's or real players. To prevent economic problems, limits will be set to the prices of certain items.
#### 3.2.7. Screen Flow
SOS will have a feature called Free Roaming. As such, the player can go wherever they want and never wait for the hardware to load the game. We want to minimize much as possible interrupting the player due to the hardware having to load certain areas.
#### 3.3. Game Options
SOS will feature lots of graphical options, with more freedom than other games. Other options (sound, controls, etc.) will be personalizable as well.
### 3.4. Replaying and Saving
SOS is a MMORPG. As such, the player's account is saved in the game server's. Actions that the player do cannot be undone (items dropped, quest completed, and such).
### 3.5. Cheats and Easter Eggs
We want to feature many Easter Eggs in SOS, enough to entertain the player but not too many to distract him. Since it's an online game, there won't be any kind of unfair cheats.
## 4. Story, Setting and Character
### 4.1. Story and Narrative
The world of SOS features a story explaining the setting of the game. Players will be thrown into a world that is in an iminent chaos, and must fight to prevent it.

<Insert story later because the internet here sucks so much I can't open Facebook>
### 4.2. Game World
The overworld (the world outside cities) will feature many collectable objects, enemies and NPC's. The world will be as much detailed as we can, to break the monotomy of the players and to actually turn it a feature of the game: the world.
#### 4.2.1. General look and feel of world
The world will be vast and lush. It will feel alive, simply like a real world: plants grow and die, NPS's move around, etc...
#### 4.2.2. Areas
The world will feature many areas, similar to real-world biomes: Plains, Mountains, Deserts, Tundras, etc... It will also feature ficticional biomes, such as Lava Mountains, Dark Areas, and such. Cities will be designed according to the biomes.
### 4.3. Characters
Player characters will be customizable at the start of the adventure, and very limited to personalization after it being created (eg. can't change color of the eyes, various face and body features). Clothes and accessories will be avaliable for the player to decorate his character as he wants. NPC characters will be there to enrich cities and worlds. Some of them can be made to be memorable.
## 5. Levels
### 5.1. Normal Levels
SOS will feature Areas instead of Levels. Levels constrain the player to a certain area and won't allow the player to leave then until it's finished. Areas will have certain specific enemies, but the players is free to enter and leave them as he pleases.
### 5.2. Training Level
At the start of the game, the player will be isolated from others and play through a tutorial level to teach him how this world works.
## 6. Interface
### 6.1. Visual System
The interface will have a "retro" look to it. However it will be rich with information, in such a way that casual players will find the important information quickly, and hardcore players will find everything they need from the information given.
### 6.2. Control System
The game will be made to be mostly played with a keyboard + mouse. It may feature compatibility with game controllers, but for now it is not in the development stage.
### 6.3. Audio, music, sound effects
The music will feel "retro" and modern at the same time. Sound effects will simply sound "retro" style.
### 6.4. Help System
At the start of the player's adventure, there will be a tutorial to teach the player the world where he will be interacting. After it there will be plentiful of information in-game and outside of it. We want to make the player feel confortable with exploring the world and doing whatever he wants.
## 7. Artificial Intelligence
### 7.1. Opponent and Enemy AI
Normal enemies in the overworld will have a very rudimentar AI. Boss monsters, however, will be way more "inteligent", and the players must crack the bosses' patterns to find out his weak spots.
### 7.2. Non-combat and Friendly Characters
NPC's will enrich the world and will be mainly to give quests, sell items, or give information. NPC's won't help the player fight enemies.
### 7.3. Support AI
At maximum, players can have pets to help the player in fight. But NPC's won't help players to fight.
## 8. Technical 
### 8.1. Target Hardware
SOS will be firstly targeted to PC systems. It will support the 3 main OS's: Windows, Mac and Linux. Ports to mobile phones (Android/iOS) and/or consoles (PS4, Xbox One, Nintendo Switch) are not considered at the moment, mainly because they offfer less controls than a keyboard. Mobile phones have smaller screens, and as such the game's interface would have to be reworked. And consoles require us to buy a development unit to be able to port the game to them, which costs a lot of money.
### 8.2. Development hardware and software (including Game Engine)
- [Tiled Map Editor](http://www.mapeditor.org/)
- [FontSquirrel](https://www.fontsquirrel.com/)
- [AuroraFW](https://github.com/aurora-fw/)

### 8.3. Network requirements
Since SOS will be an online game, the player must have a stable network connection to enjoy the game at it's best. Lag compensation mechanics will be implemented to players which have a slow internet connection.
## 9. Game Art
This topic is [still in discussion](https://github.com/shaema/assets/issues/4).
