# Asset Guidelines

These guidelines ensure every asset that's produced by different persons is cohesive, clean, and compatible with SoS.

## Index

<!-- TOC depthFrom:4 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [General](#general)
    - [Open-source software](#open-source-software)
- [Textures](#textures)
    - [Official software](#official-software)
    - [Image properties](#image-properties)
        - [Tiles/tilesets](#tiles/tilesets)
        - [Characters](#characters)
        - [Animated textures](#animated-characters)

<!-- /TOC -->

### General

These general guidelines apply to **EVERY** kind of asset.


#### Open-source software

SoS will be an open-source game. As such, **EVERY** piece of asset must be open-source too.

This also means open-source software **MUST** be used to create said assets, and the project files **MUST** also be public.

You are allowed to use a *closed-source/commercial* software to produce assets, **AS LONG AS** it fulfills these two requirements:

- The license of the software doesn't collide with the license of SoS (e.g. doesn't impose royalties, etc...)
- The project file must be convertible to a similar open-source software.

---

### Textures

By *texture* we mean every image that is used in-game. *(Interface, NPC's, Items, Monsters, etc...)*

#### Official software

SoS will support any kind of open-source software that exports an image to *.PNG* format.

Later on development, Aurora-FW may have it's own image extension for a more optimized image format for the engine.
Nevertheless, we will create a converter from .PNG to the new format.

We suggest the following software to create textures:

- **[GIMP](https://www.gimp.org/)** (recommended to create mock-up/concept art)
- **[Aseprite](https://www.aseprite.org/)**\* (recommended to create textures and animations)

\* *Aseprite may look like it's paid, but people can compile it for free. If you want to use it, you must compile it yourself following these [instructions](https://github.com/aseprite/aseprite/blob/master/INSTALL.md).*


#### Image properties

Every texture can and *should* use alpha transparency to indicate transparency.

##### Tiles / tilesets

Every texture that represents a tile must be 32x32 long. (32 pixels wide and long)

Do **NOT** make isolated tile textures, gather them all in a tileset.

Tilesets shouldn't be longer than 512x512 pixels long. (16x16 textures = 256 textures)


#### Characters

For a character you're allowed to create a 32x64 pixels texture.

Do **NOT** add character textures into tilesets.


#### Animated textures

For animated textures (tiles, character actions, etc...), gather all frames in a **single** file, since it's more efficient.

If you make an animated texture, every frame should be sequencial:

```
123456789
```

Do **NOT** do this, it's less efficient and prone to problems:

```
1234
5678
```

Also, take note: animated textures are only **SUITABLE** for small things (water flowing, characters walking, swords swinging, etc...) For more complex things (boss fights for example), the animations will be done in real time with movement in-game.


---
